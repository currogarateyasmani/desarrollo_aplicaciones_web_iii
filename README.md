# Desarrollo Aplicaciones Web III



## Materiales

[Libro Ingeniería del Software](https://drive.google.com/file/d/1bbYmyyOccTJ9NsgwR6xe1CAFtRxUX4Cb/view?usp=sharing)

## Libros

[1. Introduccion a Laravel](https://drive.google.com/file/d/1CorC8saY_8aZUX_6PNP1b3xMGY0mzRT2/view?usp=sharing)

## Realizando muestro primer CRUD

[1. CRUD](CRUD/crud.md)

## Integrando template AdminLTE en laravel

[2. Template AdminLTE](Template/template.md)

## Creando nuestro proyecto

[3. Primer CRUD del proyecto](CRUD-Proyecto/crud_proyecto.md)