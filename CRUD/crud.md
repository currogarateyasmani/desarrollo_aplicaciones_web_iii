# CRUD en Laravel

[- Grabacion](https://senatipe-my.sharepoint.com/:v:/g/personal/rcondoriq_senati_pe/EaIN2s_o2H5OkF57siqFc1IBFf16j2vMV5PqI-0ZLNKeRQ?e=bm7TJQ)

CRUD es el acrónimo de Crear, Leer, Actualizar y Eliminar. Se utiliza a menudo en el contexto de bases de datos y aplicaciones web para hacer referencia a las operaciones básicas que se pueden realizar sobre los datos almacenados.

En el contexto de Laravel, CRUD se refiere a la capacidad que tiene una aplicación para crear nuevos registros en una base de datos, leer registros existentes, actualizar registros existentes y eliminar registros. Laravel proporciona varias herramientas y características que facilitan la implementación de operaciones CRUD en una aplicación web.

Algunas de las características de Laravel que pueden ser útiles para implementar CRUD incluyen:

El uso de controladores y vistas para separar la lógica de la interfaz de usuario.

El uso de modelos y consultas Eloquent para interactuar con la base de datos de manera sencilla y coherente.

El uso de formularios y validación de formularios para recibir y validar datos del usuario.

## Crear proyecto Laravel

Lo primero que debemos hacer para desarrollar un CRUD en Laravel es crear nuestro proyecto, en nuestro caso lo haremos usando los comandos de composer:

```sh
composer create-project laravel/laravel crud
cd crud
```

Seguidamente agregamos las librerias de desarrollo, mediante el siguiente comando:

```sh
composer require laravel/breeze --dev
php artisan breeze:install
```

Con los comandos anteriores ya tendremos nuestro proyecto creado y Laravel Breeze instalado para tener un sistema de autenticación en Laravel.


**Es necesario instalar los siguientes comandos para la parte de las vistas, pero antes debemos percatarnos que tenemos instalado node**

```sh
npm install
```

## Modelo y migración

Los modelos en Laravel son clases que se utilizan para interactuar con la base de datos y representan las tablas de la base de datos. Cada modelo se asocia con una tabla de la base de datos y puede utilizar el ORM Eloquent para realizar operaciones CRUD en esa tabla.

Las migraciones en Laravel son archivos que se utilizan para definir y modificar la estructura de las tablas de la base de datos de tu aplicación. Puedes utilizar las migraciones para crear nuevas tablas, agregar o eliminar columnas de una tabla existente y realizar otras modificaciones en la estructura de la base de datos.

## Crear usuario

```sh
php artisan tinker

$user = new App\Models\User;
$user->name = "Mateo";
$user->email = "micorreo@gmail.com";
$user->password = bcrypt("micontraseña");
$user->save();
```

Seguidamente crearemos el modelo

```sh
php artisan make:model Project -mc
```

El comando anterior habrá creado el modelo, una migración y un controlador para gestionar los proyectos en nuestra aplicación CRUD.

### buscar el archivo en la carpeta "crud/database/migrations/"

```php
<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class);
            $table->string('name', 100);
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
};
```

Nuestra migración podrá crear o eliminar la tabla projects haciendo uso del comando migrate de Laravel.

Ahora vamos a abrir nuestro modelo Project para hacer algunos ajustes:

### crud/app/Models/Project.php

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Project extends Model
{
    protected $fillable = [
        'name',
        'description',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($project) {
            $project->user_id = auth()->id();
        });
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
```

* $fillable: evitamos asignación masiva.

* método boot: mientras se esté creando un nuevo proyecto, guardamos el usuario identificado en ese momento.

* método user: establecemos la relación de un proyecto con un usuario.

Con todo lo anterior, vamos a generar nuestra base de datos con la tabla de proyectos, pero recuerda primero actualizar las credenciales de tu base de datos en el archivo .env:

```sh
php artisan migrate:fresh
```

## Rutas CRUD en Laravel

En Laravel tenemos un sistema de rutas muy potente que nos permite hacer cosas increíbles. Una de ellas es definir todas las rutas de una aplicación CRUD con una sola línea.

### Agregar la linea de código en crud/routes/web.php

```php
Route::middleware('auth')->group(function () {
    
    Route::resource('projects', ProjectController::class);
});
```

El método resource generará los siguientes endpoints en nuestra aplicación:

```sh
GET|HEAD        projects .............................................. projects.index › ProjectController@index
POST            projects .............................................. projects.store › ProjectController@store
GET|HEAD        projects/create ....................................... projects.create › ProjectController@create
GET|HEAD        projects/{project} .................................... projects.show › ProjectController@show
PUT|PATCH       projects/{project} .................................... projects.update › ProjectController@update
DELETE          projects/{project} .................................... projects.destroy › ProjectController@destroy
GET|HEAD        projects/{project}/edit ............................... projects.edit › ProjectController@edit
```

## Actualizar la navegación de Laravel

Antes de seguir, y para tenerlo todo en orden, vamos a añadir la navegación al listado de proyectos en el archivo

### crud/resources/views/layouts/navigation.blade.php:

```php
<!-- Navigation Links -->
<div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
    <x-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
        {{ __('Dashboard') }}
    </x-nav-link>

    <!-- añadimos la navegación a proyectos -->
    <x-nav-link :href="route('projects.index')" :active="request()->routeIs('projects.index')">
        {{ __('Proyectos') }}
    </x-nav-link>
</div>
```

## CRUD Laravel: Listar proyectos con paginación

Vamos a añadir el método index a nuestro controlador ProjectController para obtener los proyectos paginados y así poder mostrarlos en el listado de proyectos:

### crud/app/http/Controllers/ProjectController.php

```php
<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Contracts\Support\Renderable;

class ProjectController extends Controller
{
    public function index(): Renderable
    {
        $projects = Project::paginate();
        return view('projects.index', compact('projects'));
    }
}
```

Nuestro método index simplemente obtiene los proyectos paginados utilizando Eloquent y retorna una vista que contendrá esa información para poder mostrarla, creemos esa vista:

**Crearemos una carpeta "projects" dentro del directorio resources**

### crud/resources/views/projects/index.blade.php

```php
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Proyectos') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="flex justify-between">
                        <h1 class="text-2xl font-bold">{{ __('Proyectos') }}</h1>
                        <a href="{{ route('projects.create') }}" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded">Crear proyecto</a>
                    </div>
                    <div class="mt-4">
                        <table class="table-auto w-full">
                            <thead class="text-xs font-semibold uppercase text-gray-400 bg-gray-50">
                                <tr>
                                    <th class="px-4 py-2">{{ __('Nombre') }}</th>
                                    <th class="px-4 py-2">{{ __('Descripción') }}</th>
                                    <th class="px-4 py-2">{{ __('Acciones') }}</th>
                                </tr>
                            </thead>
                            <tbody class="text-sm divide-y divide-gray-100">
                                @forelse ($projects as $project)
                                    <tr>
                                        <td class="border px-4 py-2">{{ $project->name }}</td>
                                        <td class="border px-4 py-2">{{ $project->description }}</td>
                                        <td class="border px-4 py-2" style="width: 260px">
                                            <a href="{{ route('projects.show', $project) }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">{{ __('Ver') }}</a>
                                            <a href="{{ route('projects.edit', $project) }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">{{ __('Editar') }}</a>
                                            <form action="{{ route('projects.destroy', $project) }}" method="POST" class="inline">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">{{ __('Eliminar') }}</button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="bg-red-400 text-white text-center">
                                        <td colspan="3" class="border px-4 py-2">{{ __('No hay proyectos para mostrar') }}</td>
                                    </tr>
                                @endforelse
                            </tbody>
                            @if ($projects->hasPages())
                                <tfoot class="text-xs font-semibold uppercase text-gray-400 bg-gray-50">
                                    <tr>
                                        <td colspan="3" class="border px-4 py-2">
                                            {{ $projects->links() }}
                                        </td>
                                    </tr>
                                </tfoot>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
```

![1](CRUD/1.png)

* @forelse: si existen proyectos, los recorremos, en otro caso mostramos un mensaje conforme no hay datos.

* formulario para eliminar: esta es la mejor forma de llevar a cabo un borrado a través de una interfaz en Laravel, formulario con @csrf y @method DELETE.

* $projects->hasPages(): comprueba si existen páginas en la paginación para mostrar los enlaces.

* $projects->links(): pinta los enlaces de la paginación, por defecto con Tailwind CSS.

![2](CRUD/2.md)

# CRUD Laravel: Crear proyectos

Vamos a añadir el método create a nuestro controlador ProjectController para mostrar un formulario que nos permita crear nuevos proyectos:

### crud/app/http/Controllers/ProjectController.php

```php
<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Contracts\Support\Renderable;

class ProjectController extends Controller
{
    // ...

    public function create(): Renderable
    {
        $project = new Project;
        $title = __('Crear proyecto');
        $action = route('projects.store');
        $buttonText = __('Crear proyecto');
        return view('projects.form', compact('project', 'title', 'action', 'buttonText'));
    }
}
```

* Utilizamos el sistema de traducciones de Laravel.

* Generamos los datos necesarios para la vista.

* No creamos diferentes archivos para crear o editar, creamos un form.

Ahora vamos a crear nuevo archivo form.blade.php el cual representará un formulario de alta y edición en Laravel para gestionar nuestros proyectos:

### crud/resources/views/projects/form.blade.php

```php
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $title }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="{{ $action }}" method="POST">
                        @csrf
                        @isset ($method)
                            @method($method)
                        @endif
                        <div class="mb-4">
                            <label for="name" class="block text-gray-700 text-sm font-bold mb-2">{{ __('Nombre') }}</label>
                            <input type="text" name="name" id="name" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" value="{{ old('name', $project->name) }}">
                            @error('name')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label for="description" class="block text-gray-700 text-sm font-bold mb-2">{{ __('Descripción') }}</label>
                            <textarea name="description" id="description" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">{{ old('description', $project->description) }}</textarea>
                            @error('description')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="flex justify-end">
                            <a href="{{ route('projects.index') }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">{{ __('Cancelar') }}</a>
                            <button type="submit" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded ml-2">{{ $buttonText }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
```

* action: definimos la acción que debe ejecutar el formulario, en este caso lanzará el formulario contra el endpoint store de nuestro controlador.

* @isset: lo mismo que la función isset de PHP.

* @method: Un hack de Laravel para poder procesar formularios utilizando método PUT o DELETE, útil para la actualización.

* @error: Si existe algún error con el campo dado, se mostrará en pantalla.

![3](CRUD/3.png)

Sin duda alguna lo mejor de nuestro formulario es que servirá tanto para crear como para editar.

## CRUD Laravel: Validar y guardar proyectos en base de datos

Ahora añadamos el método store a nuestro ProjectController para validar y dar de alta los proyectos en base de datos:

### crud/app/http/Controllers/ProjectController.php

```php
<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    // ...

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|unique:projects,name|string|max:100',
            'description' => 'required|string|max:1000',
        ]);
        Project::create([
            'name' => $request->string('name'),
            'description' => $request->string('description'),
        ]);
        
        return redirect()->route('projects.index');
    }
```

* $request->validate: Con este método podemos llevar a cabo la validación de los datos desde los controladores.

* Project::create: El método create de los modelos de Eloquent nos permite crear nuevos registros en nuestra base de datos.

Con lo anterior podrás dar de alta nuevos proyectos validando los datos para evitar errores en tus proyectos.

## CRUD Laravel: Detalle de un proyecto

Ahora vamos a añadir el método show a nuestro controlador para poder mostrar el detalle de un proyecto en una nueva página:

### crud/app/http/Controllers/ProjectController.php

```php
<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Contracts\Support\Renderable;

class ProjectController extends Controller
{
    // ...

    public function show(Project $project): Renderable
    {
        $project->load('user:id,name');
        return view('projects.show', compact('project'));
    }
```

* show(Project $project): Route Model Binding, de esta forma Laravel a través de Eloquent nos ofrece los datos automáticamente, ahí está el proyecto que deseamos mostrar.

* load('user:id,name'): Es una forma de especificar la cláusula select en la relación user a nuestro modelo Project, así evitamos traer más información de la necesaria.

Crea un archivo show.blade.php y vamos a mostrar el detalle de un proyecto:

### crud/resources/views/projects/show.blade.php

```php
<x-app-layout>
    <!-- detalle del proyecto con título, descripción y autor -->
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Detalle del proyecto') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1 class="text-2xl font-bold mb-4">{{ $project->name }}</h1>
                    <p class="text-gray-700 mb-4">
                        {{ __('Autor') }}: {{ $project->user->name }}
                    </p>
                    <p class="text-gray-700 mb-4">{{ $project->description }}</p>
                    <p class="text-gray-700">{{ $project->created_at->diffForHumans() }}</p>
                </div>
            </div>
        </div>

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mt-4">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{ route('projects.index') }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded ml-2">{{ __('Volver') }}</a>
                    <a href="{{ route('projects.edit', $project) }}" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded">{{ __('Editar') }}</a>
                    <form action="{{ route('projects.destroy', $project) }}" method="POST" class="inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded ml-2">{{ __('Eliminar') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
```

![4](CRUD/4.png)

## CRUD Laravel: Editar proyectos

Vamos a añadir el método edit a nuestro controlador ProjectController para mostrar un formulario que nos permita editar proyectos existentes:

### crud/app/http/Controllers/ProjectController.php

```php
<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Contracts\Support\Renderable;

class ProjectController extends Controller
{
    // ...
    
    public function edit(Project $project): Renderable
    {
        $title = __('Editar proyecto');
        $action = route('projects.update', $project);
        $buttonText = __('Actualizar proyecto');
        $method = 'PUT';
        return view('projects.form', compact('project', 'title', 'action', 'buttonText', 'method'));
    }
```

Como puedes ver, la única diferencia con el método create de nuestro controlador es que ahora estamos definiendo la variable $method, la cual hará saber a nuestro formulario que estamos editando un proyecto.

## CRUD Laravel: Validar y actualizar proyectos

Ahora añadamos el método update a nuestro ProjectController para validar y actualizar los proyectos en base de datos:

### crud/app/http/Controllers/ProjectController.php

```php
<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    // ...

    public function update(Request $request, Project $project): RedirectResponse
    {
        $request->validate([
            'name' => 'required|unique:projects,name,' . $project->id . '|string|max:100',
            'description' => 'required|string|max:1000',
        ]);
        $project->update([
            'name' => $request->string('name'),
            'description' => $request->string('description'),
        ]);
        
        return redirect()->route('projects.index');
    }
```

Muy parecido a nuestro método store anterior, la única diferencia es que ajustamos la validación para evitar que sean únicos los proyectos excluyendo el que estamos editando y en lugar de llamar al método create, llamamos al método update.

## CRUD Laravel: Eliminar proyectos

Para finalizar nuestra aplicación vamos a añadir el método destroy a nuestro ProjectController, con esto podremos eliminar proyectos y habremos finalizado este tutorial:

### crud/app/http/Controllers/ProjectController.php

```php
<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\RedirectResponse;

class ProjectController extends Controller
{
    // ...
    
    
    public function destroy(Project $project): RedirectResponse
    {
        $project->delete();
        return redirect()->route('projects.index');
    }
}
```

Llamando al método delete de nuestro modelo, Eloquent borrará este registro de nuestra base de datos y nuestro trabajo habrá finalizado.

